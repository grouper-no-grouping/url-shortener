package com.awbriggs.shortener;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.awbriggs.shortener.domain.UrlShortenRequest;
import com.awbriggs.shortener.setup.SystemActiveProfileResolver;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles(resolver = SystemActiveProfileResolver.class)
public class ApplicationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void postLinksWithInvalidLinkShouldGiveAFailure() {
        HttpEntity<UrlShortenRequest> httpEntity = new HttpEntity<>(new UrlShortenRequest("test"));
        ResponseEntity<String> response = this.restTemplate.postForEntity("/link", httpEntity, String.class);
        response.getStatusCode().is4xxClientError();
    }

    @Test
    public void postLinksWithInvalidLinkShouldFailAndGiveResponse() {
        HttpEntity<UrlShortenRequest> httpEntity = new HttpEntity<>(new UrlShortenRequest("test"));
        ResponseEntity<String> response = this.restTemplate.postForEntity("/link", httpEntity, String.class);
        response.getStatusCode().is4xxClientError();
    }

    @Test
    public void postMultipleOfSameLinkShouldGiveDifferentLinks() throws Exception {
        UrlShortenRequest request = new UrlShortenRequest("https://www.google.com");
        request.setMinutesToExist(80);
        HttpEntity<UrlShortenRequest> httpEntity = new HttpEntity<>(request);
        String firstResponse = this.restTemplate.postForObject("/link", httpEntity, String.class);
        String secondResponse = this.restTemplate.postForObject("/link", httpEntity, String.class);
        assertNotEquals(new JSONObject(firstResponse).get("newUrl"), new JSONObject(secondResponse).get("newUrl"));
    }

    @Test
    public void deleteLinkShouldRemoveLink() throws Exception {
        UrlShortenRequest urlShortenRequest = new UrlShortenRequest("https://www.google.com");
        urlShortenRequest.setMinutesToExist(80);
        HttpEntity<UrlShortenRequest> request = new HttpEntity<>(urlShortenRequest);

        String response = getNewUrlFromString(this.restTemplate.postForObject("/link", request, String.class));
        this.restTemplate.delete("/link/" + getLinkKeyFromLink(response));

        ResponseEntity<String> secondResponse = this.restTemplate.getForEntity("/link/" +
                getLinkKeyFromLink(response), null, String.class);
        assertEquals("/notfound", secondResponse.getHeaders().getLocation().getPath());
    }

    @Test
    public void postLinkForAShortTimeAndCheckThatItsNotFoundAfter() throws Exception {
        UrlShortenRequest urlShortenRequest = new UrlShortenRequest("https://www.google.com");
        urlShortenRequest.setMinutesToExist(1);
        HttpEntity<UrlShortenRequest> httpEntity = new HttpEntity<>(urlShortenRequest);
        String firstResponse = this.restTemplate.postForObject("/link", httpEntity, String.class);

        Thread.sleep(121000);
        ResponseEntity<String> response = this.restTemplate.getForEntity("/link/" +
                getLinkKeyFromLink(getNewUrlFromString(firstResponse)), null, String.class);
        assertEquals("/notfound", response.getHeaders().getLocation().getPath());
    }

    @Test
    public void postFailureSinceShouldNotExist() {
        ResponseEntity<String> responseEntity = this.restTemplate.getForEntity("/link/onoitsameme", null, String.class);
        assertEquals("/notfound", responseEntity.getHeaders().getLocation().getPath());
    }

    private String getNewUrlFromString(String response) throws JSONException {
        return new JSONObject(response).getString("newUrl");
    }

    private String getLinkKeyFromLink(String url) {
        String[] strings = url.split("/");
        return strings[strings.length - 1];
    }

}
