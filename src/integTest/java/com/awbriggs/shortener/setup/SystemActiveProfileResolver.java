package com.awbriggs.shortener.setup;

import org.springframework.test.context.ActiveProfilesResolver;

public class SystemActiveProfileResolver implements ActiveProfilesResolver {

    @Override
    public String[] resolve(Class<?> aClass) {
        final String springProfileKey = "spring.profiles.active";

        String profile = System.getProperty(springProfileKey, "");
        return profile.isEmpty() ? new String[]{} : profile.split(",");
    }

}