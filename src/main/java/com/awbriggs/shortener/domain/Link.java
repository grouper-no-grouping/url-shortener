package com.awbriggs.shortener.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.springframework.util.Assert;

public class Link implements Serializable {
    private static final long serialVersionUID = 1L;

    private String url;
    private String contentType;
    private LocalDateTime dateOfCreation;
    private long secondsOfExistence;

    public Link() {

    }

    private Link(LinkBuilder linkBuilder) {
        this.url = reformatToHttps(linkBuilder.getUrl());
        this.contentType = linkBuilder.getContentType();
        this.dateOfCreation = linkBuilder.getDateOfCreation();
        this.secondsOfExistence = linkBuilder.getTimeOfExistence();
    }

    public long getDuration() {
        return this.secondsOfExistence;
    }

    public String getUrl() {
        return url;
    }

    public LocalDateTime getDateOfCreation() {
        return dateOfCreation;
    }

    public LocalDateTime getDateOfDestruction() {
        return dateOfCreation.plusSeconds(this.secondsOfExistence);
    }

    public String getContentType() {
        return contentType;
    }

    public void setDuration(final long duration) {
        this.secondsOfExistence = duration;
    }

    private String reformatToHttps(String url) {
        if (url.contains("http://") || url.contains("https://")) {
            return url;
        }

        return "https://" + url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public void setContentType(final String contentType) {
        this.contentType = contentType;
    }

    public void setDateOfCreation(final LocalDateTime dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public static class LinkBuilder {
        private String url;
        private String contentType;
        private LocalDateTime dateOfCreation = LocalDateTime.now();
        private long timeOfExistence;

        public static LinkBuilder create() {
            return new LinkBuilder();
        }

        public String getUrl() {
            return url;
        }

        public LinkBuilder withUrl(final String url) {
            Assert.notNull(url, "Cannot have a link with no url");
            this.url = url;
            return this;
        }

        public String getContentType() {
            return contentType;
        }

        public LinkBuilder withContentType(final String contentType) {
            this.contentType = contentType;
            return this;
        }

        public LocalDateTime getDateOfCreation() {
            return dateOfCreation;
        }

        public long getTimeOfExistence() {
            return timeOfExistence;
        }

        public LinkBuilder withTimeOfExistence(final long timeOfExistence) {
            this.timeOfExistence = timeOfExistence;
            return this;
        }

        public LinkBuilder withTimeOfExistence(final long timeOfExistence, TimeUnit timeUnit) {
            this.timeOfExistence = TimeUnit.SECONDS.convert(timeOfExistence, timeUnit);
            return this;
        }

        public Link build() {
            return new Link(this);
        }
    }

}
