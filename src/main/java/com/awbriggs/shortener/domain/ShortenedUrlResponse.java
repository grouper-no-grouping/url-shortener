package com.awbriggs.shortener.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ShortenedUrlResponse {
    private final String domain;
    private final String newUrl;
    private final Errors error;

    public ShortenedUrlResponse(Errors error) {
        this.error = error;
        this.domain = "";
        this.newUrl = "";
    }

    public ShortenedUrlResponse(String newUrl, String domain) {
        this.newUrl = newUrl;
        this.domain = domain;
        this.error = null;
    }

    public String getNewUrl() {
        return (error == null) ? String.format("%s/link/%s", domain, newUrl) : "";
    }

    public String getError() {
        if (hasError()) {
            return this.error.getErrorDescription();
        }
        return null;
    }

    public boolean hasError() {
        return this.error != null;
    }

}
