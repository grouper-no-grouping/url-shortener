package com.awbriggs.shortener.domain;

public final class TimeConstants {
    public static final int MINUTES_IN_HOUR = 60;
    public static final int HOURS_IN_DAY = 24;
    public static final int DAYS_IN_WEEK = 7;

    public static final int MINUTES_IN_DAY = MINUTES_IN_HOUR * HOURS_IN_DAY;
    public static final int MINUTES_IN_WEEK = MINUTES_IN_HOUR * HOURS_IN_DAY * DAYS_IN_WEEK;

}
