package com.awbriggs.shortener.keys;

import java.util.function.Supplier;

public interface LinkKeyGenerator extends Supplier<String> {
}
