package com.awbriggs.shortener.configuration;

import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

@Configuration
@Profile("mongo")
public class MongoConfiguration {

    private static final String LINK_DATABASE = "link";

    @Bean
    public MongoDatabase mongoDatabase(@Value("${MONGO_URL:mongodb://localhost}") final String mongoURL) {
        CodecRegistry registry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoClientSettings settings = MongoClientSettings.builder()
                .codecRegistry(registry)
                .applyConnectionString(new ConnectionString(mongoURL))
                .build();
        return MongoClients.create(settings).getDatabase(LINK_DATABASE);
    }

}
