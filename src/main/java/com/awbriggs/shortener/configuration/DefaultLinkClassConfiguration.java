package com.awbriggs.shortener.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.RedisTemplate;

import com.awbriggs.shortener.keys.LinkKeyGenerator;
import com.awbriggs.shortener.keys.RandomLetters;
import com.awbriggs.shortener.keys.RandomWords;
import com.awbriggs.shortener.storage.InMemoryLinkStorage;
import com.awbriggs.shortener.storage.LinkStorage;
import com.awbriggs.shortener.storage.MongoDBLinkStorage;
import com.awbriggs.shortener.storage.RedisLinkStorage;
import com.mongodb.client.MongoDatabase;

@Configuration
public class DefaultLinkClassConfiguration {

    @Bean
    @Profile("redis")
    public LinkStorage getRedisLinkStorage(final RedisTemplate<String, Object> redisTemplate, final LinkKeyGenerator linkKeyGenerator) {
        return new RedisLinkStorage(redisTemplate, linkKeyGenerator);
    }

    @Bean
    @Profile("mongo")
    public LinkStorage getMongoLinkStorage(final MongoDatabase database, final LinkKeyGenerator linkKeyGenerator) {
        return new MongoDBLinkStorage(database, linkKeyGenerator);
    }

    @Bean
    @ConditionalOnMissingBean(LinkStorage.class)
    public LinkStorage getLinkStorage(final LinkKeyGenerator linkKeyGenerator) {
        return new InMemoryLinkStorage(linkKeyGenerator);
    }

    @Bean
    @Profile("word-keys")
    public LinkKeyGenerator getDictionaryWords() {
        return new RandomWords(null, null);
    }

    @Bean
    @ConditionalOnMissingBean(LinkKeyGenerator.class)
    public LinkKeyGenerator generator() {
        return new RandomLetters();
    }


}
