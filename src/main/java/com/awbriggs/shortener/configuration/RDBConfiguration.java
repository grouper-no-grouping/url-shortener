package com.awbriggs.shortener.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("postgres")
public class RDBConfiguration {

    @Bean
    public DataSource getConnection(@Value("${postgres.url}") final String url, @Value("${postgres.username}") final String username,
                                    @Value("${postgres.password}") final String password) {
        return DataSourceBuilder.create().url(url)
                .username(username)
                .password(password)
                .driverClassName("org.postgresql.Driver")
                .build();
    }

}
