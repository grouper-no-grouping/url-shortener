package com.awbriggs.shortener.handler;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.awbriggs.shortener.configuration.ApplicationProperties;
import com.awbriggs.shortener.domain.Errors;
import com.awbriggs.shortener.domain.Link;
import com.awbriggs.shortener.domain.ShortenedUrlResponse;
import com.awbriggs.shortener.storage.FileStorage;
import com.awbriggs.shortener.storage.LinkStorage;
import com.awbriggs.shortener.utils.CommonPatterns;

@Component
public class UrlHandler {
    private final LinkStorage linkStorage;
    private final FileStorage fileStorage;
    private final ApplicationProperties applicationProperties;

    public UrlHandler(LinkStorage linkStorage, ApplicationProperties applicationProperties, FileStorage fileStorage) {
        this.linkStorage = linkStorage;
        this.applicationProperties = applicationProperties;
        this.fileStorage = fileStorage;
    }

    public ShortenedUrlResponse getNewUrl(String url, int existenceTime) {
        if (isValidUrlLink(url)) {
            Link link = Link.LinkBuilder.create()
                    .withUrl(url)
                    .withTimeOfExistence(existenceTime, TimeUnit.MINUTES)
                    .build();
            return new ShortenedUrlResponse(linkStorage.addLink(link), applicationProperties.getDomain());
        }
        return new ShortenedUrlResponse(Errors.LINK_CREATION_ERROR);
    }

    public ShortenedUrlResponse getNewUrl(MultipartFile multipartFile, int existenceTime) {
        Optional<String> url = fileStorage.storeFile(multipartFile, existenceTime);
        if (url.isEmpty()) {
            return new ShortenedUrlResponse(Errors.LINK_CREATION_ERROR);
        }
        Link link = Link.LinkBuilder.create()
                .withUrl(url.get())
                .withTimeOfExistence(existenceTime, TimeUnit.MINUTES)
                .withContentType(multipartFile.getContentType())
                .build();
        return new ShortenedUrlResponse(linkStorage.addLink(link), applicationProperties.getDomain());
    }

    public boolean deleteUrl(String linkId) {
        return linkStorage.deleteLink(linkId);
    }

    public Link getExistingUrl(String linkId) {
        return linkStorage.getLink(linkId);
    }

    boolean isValidUrlLink(String url) {
        return CommonPatterns.IS_LINK.getPattern().matcher(url).matches();
    }

}
