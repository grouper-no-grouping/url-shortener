package com.awbriggs.shortener.utils;

import java.util.regex.Pattern;

public enum CommonPatterns {
    IS_LINK("^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$"),
    IS_WORD("\\w+");

    private String regex;
    public Pattern pattern;

    CommonPatterns(String regex) {
        this.pattern = Pattern.compile(regex);
        this.regex = regex;
    }

    public Pattern getPattern() {
        return this.pattern;
    }

    public String getRegex() {
        return this.regex;
    }
}
