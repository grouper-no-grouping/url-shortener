package com.awbriggs.shortener.controllers;

import static com.awbriggs.shortener.domain.TimeConstants.MINUTES_IN_DAY;
import static com.awbriggs.shortener.domain.TimeConstants.MINUTES_IN_HOUR;
import static com.awbriggs.shortener.domain.TimeConstants.MINUTES_IN_WEEK;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.awbriggs.shortener.domain.ShortenedUrlResponse;
import com.awbriggs.shortener.domain.UrlShortenRequest;
import com.awbriggs.shortener.handler.UrlHandler;

@RestController
public class UrlController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private UrlHandler urlHandler;

    public UrlController(UrlHandler handler) {
        this.urlHandler = handler;
    }

    @PostMapping("/link")
    public HttpEntity<ShortenedUrlResponse> addNewLink(@Valid @RequestBody UrlShortenRequest urlShortenRequest) {
        ShortenedUrlResponse urlResponse = urlHandler.getNewUrl(urlShortenRequest.getOldUrl(), urlShortenRequest.getMinutesToExist());
        if (!urlResponse.hasError()) {
            return new ResponseEntity<>(urlResponse, HttpStatus.CREATED);
        }
        logger.warn("Error processing: Url: {}, MinutesToExist: {}", urlShortenRequest.getOldUrl(), urlShortenRequest.getMinutesToExist());
        return new ResponseEntity<>(urlResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DeleteMapping("/link/{linkId}")
    public HttpEntity<?> deleteExistingLink(@PathVariable("linkId") @NotNull String linkId) {
        if (urlHandler.deleteUrl(linkId)) {
            return new HttpEntity<>(HttpStatus.OK);
        }
        logger.info("Not Found request for: {}", linkId);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/link/file")
    @Transactional(timeout = 120)
    public HttpEntity<?> handleFileUpload(@RequestParam("file") @NotNull MultipartFile file,
                                          @RequestParam(value = "minutesToExist", defaultValue = "" + MINUTES_IN_DAY) @Min(MINUTES_IN_HOUR) @Max(MINUTES_IN_WEEK) Integer minutesToExist) {
        return new ResponseEntity<>(urlHandler.getNewUrl(file, minutesToExist), HttpStatus.CREATED);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public HttpEntity<?> handleInvalidArguments() {
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
}
