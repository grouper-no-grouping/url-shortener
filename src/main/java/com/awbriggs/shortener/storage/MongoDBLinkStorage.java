package com.awbriggs.shortener.storage;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import org.springframework.scheduling.annotation.Scheduled;

import com.awbriggs.shortener.domain.Link;
import com.awbriggs.shortener.keys.LinkKeyGenerator;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBLinkStorage implements LinkStorage {

    private static final String LINK_COLLECTION = "link";
    private final MongoCollection<MongoLink> collection;
    private final LinkKeyGenerator linkKeyGenerator;

    public MongoDBLinkStorage(final MongoDatabase database, final LinkKeyGenerator linkKeyGenerator) {
        this.collection = database.getCollection(LINK_COLLECTION, MongoLink.class);
        this.linkKeyGenerator = linkKeyGenerator;
    }

    @Override
    public Link getLink(final String id) {
        MongoLink searchedLink = this.collection.find(eq("linkid", id), MongoLink.class).first();
        return (searchedLink == null) ? null : searchedLink.getLink();
    }

    @Override
    public String addLink(final Link item) {
        MongoLink mongoLink = new MongoLink(getUniqueLinkKey(), item, 0);
        this.collection.insertOne(mongoLink);

        return mongoLink.getLinkid();
    }

    @Override
    public boolean deleteLink(final String id) {
        return this.collection.deleteOne(eq("linkid", id)).wasAcknowledged();
    }

    @Scheduled(cron = "${link.expiration.cron}")
    public void cleanUpLinks() {
        this.collection.deleteMany(gt("link.destructionTime", LocalDateTime.now()));
    }


    //TODO: Better options? Technically it's not handling things correctly. Exceptions, maybe?
    private String getUniqueLinkKey() {
        return Stream.generate(linkKeyGenerator)
                .limit(50)
                .filter((possibleKey) -> this.collection.find(eq("linkid", possibleKey)).first() == null)
                .findFirst()
                .orElse(null);
    }

}
