package com.awbriggs.shortener.storage;

import com.awbriggs.shortener.domain.Link;

public class MongoLink {
    private String linkid;
    private Link link;
    private int viewCount;

    public MongoLink() {

    }

    public MongoLink(final String linkid, final Link link, final int viewCount) {
        this.linkid = linkid;
        this.link = link;
        this.viewCount = viewCount;
    }

    public String getLinkid() {
        return linkid;
    }

    public Link getLink() {
        return link;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setLinkid(final String linkid) {
        this.linkid = linkid;
    }

    public void setLink(final Link link) {
        this.link = link;
    }

    public void setViewCount(final int viewCount) {
        this.viewCount = viewCount;
    }

}
