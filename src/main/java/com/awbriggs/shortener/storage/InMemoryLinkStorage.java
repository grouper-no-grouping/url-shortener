package com.awbriggs.shortener.storage;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.springframework.scheduling.annotation.Scheduled;

import com.awbriggs.shortener.domain.Link;
import com.awbriggs.shortener.keys.LinkKeyGenerator;

public class InMemoryLinkStorage implements LinkStorage {
    private final LinkKeyGenerator linkKeyGenerator;
    private final Map<String, Link> idToLinkMap;

    public InMemoryLinkStorage(final LinkKeyGenerator linkKeyGenerator) {
        this.idToLinkMap = new ConcurrentHashMap<>();
        this.linkKeyGenerator = linkKeyGenerator;
    }

    public Link getLink(String linkId) {
        if (idToLinkMap.containsKey(linkId)) {
            return idToLinkMap.get(linkId);
        }

        return null;
    }

    @Override
    public String addLink(final Link link) {
        Optional<String> key = Stream.generate(linkKeyGenerator)
                .filter(id -> !idToLinkMap.containsKey(id))
                .findFirst();

        if (key.isPresent()) {
            this.idToLinkMap.put(key.get(), link);
            return key.get();
        }

        return null;
    }

    @Override
    public boolean deleteLink(String id) {
        return (idToLinkMap.remove(id) != null);
    }

    @Scheduled(cron = "${link.expiration.cron}")
    public void cleanUpLinks() {
        idToLinkMap.forEach((key, value) -> {
            if (LocalDateTime.now().isAfter(value.getDateOfDestruction())) {
                idToLinkMap.remove(key);
            }
        });
    }

}
