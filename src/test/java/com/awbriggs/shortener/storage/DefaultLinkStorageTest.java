package com.awbriggs.shortener.storage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.awbriggs.shortener.domain.Link;
import com.awbriggs.shortener.keys.RandomLetters;

public class DefaultLinkStorageTest {

    @Test
    public void testThatLinksArePurgedCorrectly() throws Exception {
        InMemoryLinkStorage linkMapper = new InMemoryLinkStorage(new RandomLetters());
        String id = linkMapper.addLink(Link.LinkBuilder.create().withUrl("test").withTimeOfExistence(10).build());
        Thread.sleep(15000);
        linkMapper.cleanUpLinks();
        assertNull(linkMapper.getLink(id));
    }

    @Test
    public void reformatDefaultLinkStorageIfNeededDoesForHttps() {
        InMemoryLinkStorage linkMapper = new InMemoryLinkStorage(new RandomLetters());
        String id = linkMapper.addLink(Link.LinkBuilder.create().withUrl("https://www.google.com").withTimeOfExistence(5, TimeUnit.MINUTES).build());
        assertEquals("https://www.google.com", linkMapper.getLink(id).getUrl());
    }

    @Test
    public void reformatDefaultLinkStorageIfNeededDoesForHttp() {
        InMemoryLinkStorage linkMapper = new InMemoryLinkStorage(new RandomLetters());
        String id = linkMapper.addLink(Link.LinkBuilder.create().withUrl("https://www.google.com").withTimeOfExistence(5, TimeUnit.MINUTES).build());
        assertEquals("https://www.google.com", linkMapper.getLink(id).getUrl());
    }

    @Test
    public void linkIsDeleted() {
        var linkMapper = new InMemoryLinkStorage(new RandomLetters());
        var link = linkMapper.addLink(Link.LinkBuilder.create().withUrl("https://www.google.com").withTimeOfExistence(5, TimeUnit.MINUTES).build());
        assertNotNull(link);
        linkMapper.deleteLink(link);
        assertNull(linkMapper.getLink(link));
    }

}
