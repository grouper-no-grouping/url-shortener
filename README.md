URL-Shortener
-------------

Just needed something to shorten some urls for me. 


Running Locally:

`./gradlew bootRun`


Environment Variables


| Variable | Use| Required |
| ---- | ---- | ----       |
| S3_Key      |      S3 Key for image storage | Yes, if wanting to use S3           |
| S3_Secret |  S3 Secret for image storage    | Yes if wanting to use S3           |


Deploying:

I personally use dokku on an DO droplet. Here's a link for that: https://www.digitalocean.com/docs/one-clicks/dokku/


Usage:

THe normal gui is fine, but there's some examples in personal_scripts if you need something more
